# AVIATUR TECHNICAL TEST

_This little project is about a technical test for the company **Aviatur** for the position:
> **Web developer.**

## Installation and initialization 🔧

_If you have already cloned the project code, you must install:_

```
npm install -g @ionic/cli 
```
_To build the dependencies for the project in developer mode use the script:_

```
npm install
``` 
_To start the project in developer mode use the script:_

```
ionic serve --external
```

## Built with 🛠️

- [Angular](https://angular.io/)
- [Ionic v5](https://ionic.io/)
- [Tailwind](https://tailwindcss.com/)
- [JSON](https://www.json.org/json-en.html)
- [SCSS](https://sass-lang.com/) 

## Deployment 💻

- https://aviaturapplication.web.app/

## Authors ✒️

- **Lina Castro** - _Frontend and backend Developer_ - [lirrums](https://gitlab.com/linacastrodev)

## License 📄

This project is under the License (MIT)

## Acknowledgements 🎁

- We greatly appreciate to the company for motivating us with the project. providing us with tools and their knowledge for our professional growth📢
