import { Component, OnInit } from "@angular/core";
import * as Data from "../../../data/data.json";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit {
  data: any = Data;
  show: boolean = false;
  isItemAvailable: any;
  constructor() {}

  ngOnInit() {
    /* THIS VARIABLE ADD THE RETURN RESULT OF THE JSON HOTELS */
    this.isItemAvailable = this.uploadData();
  }
  /* THIS FUNCTION GET LIST TO ALL JSON HOTELS */
  uploadData(): any {
    return this.data.default;
  }
  /* THIS FUNCTION SHOW AND HIDDE THE FILTER SECTION */
  showFilters() {
    this.show = !this.show;
  }
  /* FUNCTION FOR SEARCHING HOTELS */
  search(evt) {
    try {
      const event = evt.srcElement.value;
      if (!event) {
        return;
      }
      this.isItemAvailable = this.isItemAvailable.filter((item) => {
        if (item.name && event) {
          return item.name.toLowerCase().indexOf(event.toLowerCase()) > -1;
        }
      });
    } catch (error) {
      console.error(error);
    }
  }
  /* FUNCTION FOR SEARCHING HOTELS */
}
